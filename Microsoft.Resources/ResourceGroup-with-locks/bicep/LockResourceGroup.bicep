//Specify  the lock type
@allowed([
  'CannotDelete'
  'ReadOnly'
])
@description('Specify  the lock type')
param resourceGroupLock string

resource createRgLock 'Microsoft.Authorization/locks@2016-09-01' = {
  name: 'rgLock'
  properties: {
    level: resourceGroupLock
    notes: 'Resource group and its resources should not be deleted.'
  }
}
