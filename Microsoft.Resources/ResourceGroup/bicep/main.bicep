//.......................main.bicep

//Setting the target scope
targetScope = 'subscription'

//Specify the name of the resource group
param resourceGroupName string

//Specify the location of the resource group
param resourceGroupLocation string

//Specify the tags
param resourceGroupTags object

//Creating a resource group
resource rg 'Microsoft.Resources/resourceGroups@2021-01-01' = {
  name: resourceGroupName
  location:resourceGroupLocation
  tags:resourceGroupTags
}
